package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"
)

func main() {
	c := make(chan string)

	args := filterInputData(os.Args[1:])
	for _, a := range args {
		go checkURL(a, c)
	}

	for u := range c {
		go func(url string) {
			time.Sleep(5 * time.Second)
			checkURL(url, c)
		}(u)
	}
}

func filterInputData(urls []string) []string {
	if len(urls) == 0 {
		fmt.Println("I'm good at reading minds, but not that good!")
		fmt.Println("Try giving me something to work with, just like this: `go run main.go http://URL.com`")
		os.Exit(1)
	}
	var validURLs []string
	for _, u := range urls {
		if isValidURL(u) {
			validURLs = append(validURLs, u)
		} else {
			fmt.Println(u, "is not a valid URL")
			continue
		}
	}
	return validURLs
}

func isValidURL(u string) bool {
	_, err := url.ParseRequestURI(u)
	if err != nil {
		return false
	}
	return true
}

func checkURL(url string, c chan string) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error encountered on the URL", url, ":", err)
		c <- url
		return
	}
	fmt.Println(url, resp.Status)
	c <- url
}
