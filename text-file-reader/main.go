package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	args := os.Args[1:]
	for _, a := range args {
		content, err := ioutil.ReadFile(a)

		if err != nil {
			fmt.Println("Error encountered:", err)
			os.Exit(1)
		}
		os.Stdout.Write(content)
	}
}
