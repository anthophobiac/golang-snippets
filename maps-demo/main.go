package main

import "fmt"

func main() {
	// declaring a map is easy:
	colors := map[string]string{ // keys and values are to be strings
		"red":   "#ff0000",
		"green": "#00ff00",
		"blue":  "#0000ff",
	}

	// another way to create an empty map:
	tasks := make(map[string]string)
	tasks["learn about maps"] = "today"
	delete(tasks, "learn about maps")

	fmt.Println(tasks)

	printMap(colors)
}

// iterating over a map:
func printMap(c map[string]string) {
	for color, hex := range c { // for key, value
		fmt.Println("The hex color of", color, "is", hex)
	}
}
