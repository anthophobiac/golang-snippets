// print-args prints out the command line arguments, if they're passed when running the main.go file
package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args[1:]
	fmt.Println("The command you've run is", os.Args[0])
	if len(args) == 0 {
		fmt.Println("No arguments passed, nothing to print")
		os.Exit(1)
	}

	fmt.Println("You've passed the following arguments:")
	for i := 1; i < len(args); i++ {
		fmt.Println(i, os.Args[i])
	}
}
