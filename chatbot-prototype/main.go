package main

import "fmt"

type bot interface {
	getGreeting() string
}

type englishBot struct {
}

type frenchBot struct {
}

func main() {
	eb := englishBot{}
	fb := frenchBot{}

	printGreeting(eb)
	printGreeting(fb)
}

func printGreeting(b bot) {
	fmt.Println(b.getGreeting())
}

func (eb englishBot) getGreeting() string {
	return "Hello there!"
}

// the short var name for frenchBot can be ommited if it's not used
func (frenchBot) getGreeting() string {
	return "Bonjour !"
}
