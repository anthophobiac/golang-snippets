package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if len(d) != 52 {
		t.Errorf("Expected deck length of 52, but got %v", len(d))
	}

	if d[0] != "Ace of Spades" {
		t.Errorf("First card expected to be 'Ace of Spades', received: %v", d[0])
	}

	if d[len(d)-1] != "King of Clubs" {
		t.Errorf("Last card expected to be 'King of Clubs', received: %v", d[len(d)-1])
	}
}

func TestSaveToFileAndNewDeckFromFile(t *testing.T) {
	// clean up the remains of the prior tests
	os.Remove("_decktesting")

	d := newDeck()
	d.saveToFile("_decktesting")

	newDeck := newDeckFromFile("_decktesting")

	if len(newDeck) != 52 {
		t.Errorf("Expected deck length of 52, but got %v", len(newDeck))
	}
}
