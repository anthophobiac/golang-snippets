package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	urls := []string{
		"https://golang.org/doc/",
		"https://gobyexample.com/",
		"https://golang.cafe/22",
	}

	c := make(chan string)
	for _, url := range urls {
		go checkURL(url, c)
	}

	for u := range c {
		go func(url string) {
			time.Sleep(5 * time.Second)
			checkURL(url, c)
		}(u)
	}
}

func checkURL(url string, c chan string) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Error encountered on the URL", url, ":", err)
		c <- url
		return
	}
	fmt.Println(url, resp.Status)
	c <- url
}
