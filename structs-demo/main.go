package main

import "fmt"

// defining a custom type that will exist only within a program
type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contact   contactInfo
	// also possible to just declare a field `contactInfo`
	// that will stand for both the field and the type
}

func main() {
	bojack := person{
		"Bojack",
		"Horseman",
		contactInfo{
			"bojack@horseman.mail",
			55000,
		},
	}
	// the order here matters a lot!

	//another way to do this:
	todd := person{firstName: "Todd", lastName: "Chavez"}

	//and another one:
	var diane person // both fields of this struct are empty strings for now
	diane.firstName = "Diane"
	diane.lastName = "Nguyen"
	// now they're not empty

	fmt.Println(bojack.firstName, ": 'Shut up,", todd.firstName, "!'")
	fmt.Printf("%+v", diane) // this prints out the field names and values

	/* Updating the name of our person:
	1 . creating a pointer to the bojack variable:
	bojackPointer := &bojack // a reference to the struct in memory
	bojackPointer.updateFirstName("BJ")

	2. using the shortcut: 	*/
	bojack.updateFirstName("Boj")
	bojack.printInfo()
}

func (pointerToPerson *person) updateFirstName(newFirstName string) {
	// *pointer - asks for data at the address
	(*pointerToPerson).firstName = newFirstName
}

// structs with receiver function
func (p person) printInfo() {
	fmt.Printf("%+v", p)
}
