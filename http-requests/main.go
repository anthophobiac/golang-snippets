package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWriter struct{}

func main() {
	resp, err := http.Get("https://tour.golang.org/list")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	/* printing out the body of a resource:
	bs := make([]byte, 1000000)
	resp.Body.Read(bs)
	fmt.Println(string(bs))
	*/

	// a more eloquent way to achieve this:
	// io.Copy(os.Stdout, resp.Body)

	lw := logWriter{}
	io.Copy(lw, resp.Body)
}

// implementing the Writer interface ourselves
func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("The amount of bytes:", len(bs))
	return len(bs), nil
}
